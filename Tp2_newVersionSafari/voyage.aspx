﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="voyage.aspx.cs" Inherits="Tp2_newVersionSafari.voyage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="strict-origin" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style3.css" rel="stylesheet" />
 

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <title></title>
</head>
<body>
    <div class="row">
           <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

         <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="">About</a>
                                    <a class="nav-item nav-link" href="">Pages</a>
                                    <a class="nav-item nav-link " href="">Gallery</a>
                                    <a class="nav-item nav-link " href="">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a> 
                                    <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                  <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete"></i>Safari Adventure</h1>
                                </div>
                                <div id="btn" class="col-2 titre">      
                                     <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>                   
                                </div>
                            </div>
                        </div>            
                    </div>

                <%-- principal admin--%>

                  <div class ="row" id="containerContact">
                        <div class="col-12" id="infosContact">
                            <div class="row">
                                <div class="col-12">
                                    <h1 id="titreAdmin">Page Administrateur</h1>
                                    <h5>Configuration des éléments présents sur le site ce passe ici !!!</h5>
                                    <h5>Veuillez cliquez sur une des options suivantes : </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                     <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup2">
                                        <div class="navbar-nav">
                                                <a class="nav-item nav-link active" href="admin.aspx">Accueil <span class="sr-only">(current)</span></a>
                                                <a class="nav-item nav-link" href="email.aspx">Email</a>
                                                <a class="nav-item nav-link" href="voyage.aspx">Voyages</a>

                                        </div>
                                        </div>
                                    </nav>


                                      <%--Formulaire creation de voyages --%>
                                            <form id="formVoyages" runat="server">
                                            <div class="col-12">
                                               <br />
                                                <br />
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h6 id="titreVoyages">CREATION DE VOYAGES </h6>
                                                                <p id="txtVoyages">Creez ici de nouveaux voyages dans la base de données. Selectionnez une image dans le menu déroulant pour avoir un apreçu.</p>
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <asp:TextBox ID="TextBoxTitre" placeholder="Titre" runat="server"  Width="100%"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <asp:TextBox ID="TextBoxSousTitre" placeholder="Sous-Titre" runat="server"  Width="100%"></asp:TextBox>                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-12">
                                                                <asp:TextBox ID="TextBoxDescription" placeholder="Description" runat="server" TextMode="MultiLine" Width="100%" ></asp:TextBox>
                                                            </div>     
                                                        </div>
                                                <%--dropdown images --%>
                                                        <div class="form-row">
                                                            <div class="form-group col-12">
                                                                <asp:DropDownList ID="DropDownListImages" runat="server" AutoPostBack = "true" OnSelectedIndexChanged="DropDownListImages_SelectedIndexChanged" />
                                                            </div> 
                                                            <div>
                                                                <asp:Image ID="ImageVoyage" CssClass="ImageVoyage" runat="server" />
                                                            </div>

                                                        </div>
                                                    <br />
                                                  
                                              
                                                        <%--boutons --%>
                                                    <asp:Button ID="ButtonCreate" runat="server" Text="Creer ce voyage" onClick="BtnCreate_Click" />
                                                    <asp:Button ID="ButtonClear" runat="server" Text="Clear" onClick="ButtonClear_Click" />
                                                <br />
                                                
                                    <div class="row">
                                                        <br />
                                                        <br />
                <div class="col-12">
                    <hr />
                    <h5>Selectionnez les 3 voyages a mettre en page d'accueil :  </h5>
                    <br />
                    <asp:GridView ID="GridViewChoix" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="LinqDataSource1">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                            <asp:BoundField DataField="titre" HeaderText="titre" SortExpression="titre" />
                            <asp:BoundField DataField="soustitre" HeaderText="soustitre" SortExpression="soustitre" />
                            <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
                            <asp:BoundField DataField="imageNom" HeaderText="imageNom" SortExpression="imageNom" />
                            <asp:TemplateField HeaderText ="1er Item du carousel" >
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxAdd" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="Tp2_newVersionSafari.SafariDbDataContext" EntityTypeName="" TableName="voyages">
                    </asp:LinqDataSource>
                    <br />
                    <asp:Button ID="ButtonUpdate" runat="server" Text="Mettre a jour l'accueil" OnClick="ButtonUpdate_click" />

                </div>
                                                                
                                                            
                       </div>
                                                </div>
                                                
                                                </form>                            
                                    
                                    
                                    
                                    
                              </div><%-- fin row --%>
                        </div> <%-- fin col --%>
                    </div><%-- fin row --%>
                </div>
            </div>
            <div class="col-2 d-none d-lg-block tests"></div>
        </div>
    










 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>
</html>
