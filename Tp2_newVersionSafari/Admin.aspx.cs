﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Services.Description;

namespace Tp2_newVersionSafari
{
    public partial class Admin : System.Web.UI.Page
    {
        private SafariDbDataContext db = new SafariDbDataContext();
        public string image1, image2, image3, image4, image5, image6, background;
        public string home, about, pages, blog, gallery, contact, admin;
        public string titre1, titre2, titre3, titre4, titre5, titre6;
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Session3\AT1\Tp2_newVersionSafariii\Tp2_newVersionSafari\App_Data\Database.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd1 = new SqlCommand("select * from infos where Id= 1;", con);
                SqlCommand cmd2 = new SqlCommand("select * from infos where Id= 7;", con);
                SqlCommand cmd3 = new SqlCommand("select * from infos where Id= 8;", con);
                SqlCommand cmd4 = new SqlCommand("select * from infos where Id= 9;", con);




                con.Open();


                SqlDataReader reader1 = cmd1.ExecuteReader();
                while (reader1.Read())
                {
                    titre1 = reader1["titre"].ToString();

                }

                con.Close();
                con.Open();


                SqlDataReader reader2 = cmd2.ExecuteReader();
                while (reader2.Read())
                {

                    background = reader2["image"].ToString();
                }

                con.Close();
                con.Open();


                SqlDataReader reader3 = cmd3.ExecuteReader();
                while (reader3.Read())
                {

                    home = reader3["titre"].ToString();
                    about = reader3["titre2"].ToString();
                    pages = reader3["texte"].ToString();

                }

                con.Close();
                con.Open();


                SqlDataReader reader4 = cmd3.ExecuteReader();
                while (reader4.Read())
                {

                    blog = reader4["titre"].ToString();
                    gallery = reader4["titre2"].ToString();
                    contact = reader4["texte"].ToString();

                }

                con.Close();
            }
            catch (Exception ex)
            {

            }

            finally
            {
                con.Close();
            }



            //affiche les voyages
            if (!IsPostBack)
            {
                string[] ImagePaths = Directory.GetFiles(Server.MapPath("~/Images/"));
                List<ListItem> Imgs = new List<ListItem>();
                foreach (string imgPath in ImagePaths)
                {
                    string ImgName = Path.GetFileName(imgPath);
                    Imgs.Add(new ListItem(ImgName, "~/Images/" + ImgName));
                }
                GridViewImages.DataSource = Imgs;
                GridViewImages.DataBind();
            }


        }

        protected void ButtonUpload_Click(object sender, EventArgs e)
        {


            if ((File1.PostedFile != null) && (File1.PostedFile.ContentLength > 0))
            {
                string nomImage = System.IO.Path.GetFileName(File1.PostedFile.FileName);
                string SaveLocation = Server.MapPath("images") + "\\" + nomImage;

                //insertion de l'image uploadée dans la database table images
                images newimage = new images { nom = nomImage.ToString(), url = SaveLocation.ToString() };
                db.images.InsertOnSubmit(newimage);
                db.SubmitChanges();




                try
                {
                    File1.PostedFile.SaveAs(SaveLocation);
                    Response.Write("L'image a bien été ajoutée !");
                }
                catch (Exception ex)
                {
                    Response.Write("Error" + ex.Message);
                }
            }
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);

        }
        //update le carouselle avec les nouveaux voyage choisi par admin
        protected void ButtonUpdate_click(object sender, EventArgs e)
        {
           
            db.ExecuteCommand("TRUNCATE TABLE carousel");

            int compteur = 0;

            if (compteur < 4)
            {
                foreach (GridViewRow row in GridViewImages.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox checkImg = row.Cells[2].FindControl("CheckBoxAdd") as CheckBox;

                        if (checkImg.Checked)
                        {
                            string nomImage = row.Cells[0].Text;
                            carousel imgCarousel = new carousel { nom = nomImage };


                            db.carousel.InsertOnSubmit(imgCarousel);
                            db.SubmitChanges();
                            compteur += 1;



                        }

                    }

                }

                Response.Write("<script>alert('Les images ont bien été mises a jour ! ');</script>");

            }
            else
            {
                Response.Write("<script>alert('Maximum 3 images svp');</script>");
            }


        }




    }
}