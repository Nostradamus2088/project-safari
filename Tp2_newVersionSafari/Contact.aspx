﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Tp2_newVersionSafari.Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="strict-origin" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <title></title>
</head>
<body>
    <%-- <form id="form1" runat="server" class="container">--%>
        <div class="row">
            <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

                <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="">About</a>
                                    <a class="nav-item nav-link" href="">Pages</a>
                                    <a class="nav-item nav-link " href="">Gallery</a>
                                    <a class="nav-item nav-link " href="">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a>
                                <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                 <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete">Safari Adventure</i></h1>
                        </div>
                        <div id="btn" class="col-2 titre">
                            <form>
                                <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>

                            </form>
                            
                        </div>

                            </div>

                        </div>
                        
                    </div>

                 <%--Formulaire de contact --%>
                    <%--infos barre de gauche --%>
                 <div class ="row" id="containerContact">
                        <div class="col-4" id="infosContact">
                            <div class="row" id="adressRow">
                                <div class="col">
                                    <div class="row">
                                        <h6 id="titreAdress">ADRESS: </h6><br />
                                    </div>
                                    <div class="row">
                                        <p id="textAdress"><i class="fas fa-home" id="iconhome"></i> 138 Atlanta Street </p><br />
                                    </div>
                                </div>    
                            </div>

                            <div class="row" id="phonesRow">
                                <div class="col">
                                    <div class="row">
                                        <h6 id="titrePhones">PHONES: </h6><br />
                                    </div>
                                    <div class="row">
                                        <p id="textPhones"><i class="fas fa-phone-alt" id="iconPhone"></i> 900-254-6789</p><br />
                                    </div>
                                    <div class="row">
                                        <p id="textFax"><i class="fas fa-fax" id="iconFax"></i> 900-254-6789</p><br />
                                    </div>
                                </div>    
                            </div>

                            <div class="row" id="emailRow">
                                 <div class="col">
                                    <div class="row">
                                        <h6 id="titreEmail">ADRESS</h6><br />
                                    </div>
                                    <div class="row">
                                        <p id="textEmail"><i class="fas fa-envelope" id="iconEmail"></i> Email@demolink.org</p><br />
                                    </div>
                                    <div class="row">
                                        <p id="textEmail2">Download informations as </p><a id="vcardLink"href="https://en.wikipedia.org/wiki/VCard">vCard</a><br />
                                    </div>
                                </div>        
                            </div>
                        </div>

                     <%--Formulaire principal --%>
                        <div class="col-8">
                            
                                    <div class="row">
                                        <div class="col-12">
                                            <h6 id="titreContact">MISCELLANEOUS INFORMATION: </h6>
                                            <p id="textContact">Email us with any questions or inquiries or use our contact data.</p>
                                        </div>
                                    </div>
                                <form id="formContact" name="contact">
                                 <div class="form-row">
                                     <div class="form-group col-md-4">
                                            <input type="text" class="form-control" id="inputName" placeholder="Name"required />
                                     </div>
                                     <div class="form-group col-md-4">
                                            <input type="email" class="form-control" id="inputEmail" placeholder="Email"required />
                                     </div>
                                     <div class="form-group col-md-4">
                                            <input type="tel" class="form-control" id="inputTel" placeholder="Phone" required />
                                     </div>
                                 </div>

                                 <div class="form-row">
                                     <div class="form-group col-12">
                                            <textarea class="form-control" rows="3" id="inputMessage" placeholder="Message" required></textarea>
                                     </div>     
                                 </div>
                                    <%--boutons --%>
                                 <div class="row" id="boutonsRow">
                                     <input type="button" id="btnSend" value="Send" onclick="validate();" class="btn btn-warning" />
                                            
                                            <input type="reset" id="btnClear" value="Clear" class="btn btn-secondary"/>
                                           <br />
                                    
                                 </div>

                         </form>
                                    <div id="d1"></div>
                     </div>
                        
                </div>


                
                    
                    
                </div>
                <div class="col-2 d-none d-lg-block tests"></div>
   
        </div>

   
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="./script/Contact.js"></script>
</body>
</html>