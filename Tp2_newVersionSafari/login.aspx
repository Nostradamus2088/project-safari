﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Tp2_newVersionSafari.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"/>
    <link href="./style/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
    <title>Safari Adventure</title>
  </head>
  <body>
      
      
   <%-- <form id="form1" runat="server" class="container">--%>
        <div class="row">
            <img id="bg" src="./images/background.jpg" />
             <div class="col-2 d-none d-lg-block tests"></div>
             <div class="col" id="section">
                  <div class="row" id="rowNav">

            <%-- menu navbar--%>
                      <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                    <a class="nav-item nav-link active" href="Default.aspx">Home <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="">About</a>
                                    <a class="nav-item nav-link" href="">Pages</a>
                                    <a class="nav-item nav-link " href="">Gallery</a>
                                    <a class="nav-item nav-link " href="">Blog</a>
                                    <a class="nav-item nav-link " href="Contact.aspx">Contact</a> 
                                    <a class="nav-item nav-link " href="login.aspx">Admin</a> 
                            </div>
                            </div>
                        </nav>
                   </div>
                 <%--bande noire avec titre principale + bouton --%>
                   <div id="bandeTitre" class="row">
                        <div class="col-12">
                            <div class="row">
                                <div  class="col-10 titre" >
                                    
                                    <h1 id="titre"class="titre"><i class="fas fa-globe-americas" id="planete"></i>SAFARI ADVENTURE</h1>
                        </div>
                        <div id="btn" class="col-2 titre">
                            <form>
                                <button id="btnRecherche" type="submit"><i style="font-size:medium;" class="fas fa-search"></i></button>

                            </form>
                            
                        </div>

                            </div>

                        </div>
                        
                    </div>


                 <form id="formLogin" runat="server">
                    <div id="divLogin">

                        <div id="divUsername">
                            <asp:Label  ID="lblUsername" runat="server" Text="Username"/>
                            <asp:TextBox ID="txtboxUsername" runat="server" type="text"/>
                        </div>
                  
                        <div id="divPassword">
                            <asp:Label ID="lblPassword" runat="server" Text="Password"/>
                            <asp:TextBox ID="txtboxPassword" runat="server" type="password"/>
                        </div>
     
                        <asp:Button ID="btnLogin" type="submit" runat="server" Text="Sign In" OnClick="btnLogin_Click"/>
                    </div>
                </form>
          
                


                 
               
                    
                </div>
                <div class="col-2 d-none d-lg-block tests"></div>
   
        </div>
<%--      </form>--%>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>