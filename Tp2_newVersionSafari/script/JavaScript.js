﻿
$(document).ready(function () {

    $('#btnSend').click(comment);

});



function validate() {
    console.log("validate");
    var nom = document.getElementById('inputName');
    var email = document.getElementById('inputEmail');
    var tel = document.getElementById('inputTel');
    var message = document.getElementById('inputMessage');


    var u = new user(nom.value, email.value, tel.value, message.value);

    if (!u.validateNom()) {
        nom.style.borderColor = "red";

    }
    else {
        nom.style.borderColor = "lightgrey";
    }

    if (!u.validateTel()) {
        tel.style.borderColor = "red";

    }
    else {
        tel.style.borderColor = "lightgrey";
    }

    if (!u.validateEmail()) {
        email.style.borderColor = "red";

    }
    else {
        email.style.borderColor = "lightgrey";
    }

    if (!u.validateMessage()) {
        message.style.borderColor = "red";

    }
    else {
        message.style.borderColor = "lightgrey";
    }

}

function comment()
{
    var nom = document.getElementById('inputName');
    var email = document.getElementById('inputEmail');
    var tel = document.getElementById('inputTel');
    var message = document.getElementById('inputMessage');


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "insert.aspx?nm=" + nom + "&email=" + email + "&tel=" + tel + "&message=" + message + "&opr=Send", false);
    xmlhttp.send(null);

    document.getElementById('inputName').value = "";
    document.getElementById('inputEmail').value = "";
    document.getElementById('inputTel').value = "";
    document.getElementById('inputMessage').value = "";

    

}

function clear() {


    console.log("entre dans clear");
    var nom = document.getElementById('inputName');
    var email = document.getElementById('inputEmail');
    var tel = document.getElementById('inputTel');
    var message = document.getElementById('inputMessage');


    nom.style.borderColor = "lightgrey";
    email.style.borderColor = "lightgrey";
    tel.style.borderColor = "lightgrey";
    message.style.borderColor = "lightgrey";

    document.getElementById('formContact').reset();

}

function display() {


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "insert.aspx?opr=display", false);
    xmlhttp.send(null);

    document.getElementById("d1").innerHTML = xmlhttp.responseText;

}

//creation du constructeur 
function user(nom, email, tel, message) {
    this.nom = nom;
    this.email = email;
    this.tel = tel;
    this.message = message;

}
//proprietee de la classe qui
//permet de recuperer le nom
user.prototype.getNom = function () {
    return this.nom;
}
//proprietee de la classe qui
//permet de modifier le nom
user.prototype.setNom = function (nom) {
    this.nom = nom;
}

//proprietee de la classe qui
//permet de r�ccup�rer le mail
user.prototype.getMail = function () {
    return this.email;
}
//proprietee de la classe qui
//permet de modifier le mail
user.prototype.setMail = function (email) {
    this.email = email;

}

//proprietee de la classe qui
//permet de modifier le tel
user.prototype.setTel = function (tel) {
    this.tel = tel;

}
//proprietee de la classe qui
//permet de reccuperer le tel
user.prototype.getTel = function () {
    return this.tel;
}

//proprietee de la classe qui
//permet de modifier le message
user.prototype.setTel = function (message) {
    this.message = message;

}
//proprietee de la classe qui
//permet de reccuperer le message
user.prototype.getMessage = function () {
    return this.message;
}


//fonction permettant de valider le nom
user.prototype.validateNom = function () {
    var status;

    for (var i = 0; i < this.nom.length; i++) {
        if (((this.nom.charCodeAt(i) >= "A".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "Z".charCodeAt(0))) ||
            ((this.nom.charCodeAt(i) >= "a".charCodeAt(0)) && (this.nom.charCodeAt(i) <= "z".charCodeAt(0)))) {
            status = true;

        }
        else {
            status = false;
        }
    }

    return status;

}

//fonction permettant de valider le numero de tel
user.prototype.validateTel = function () {

    var telRegEx = /^(\(\d{3}\)) ?\d{3}-\d{4}/;
    if (telRegEx.test(this.tel)) {
        return true;
    }
    else {
        return false;
    }

}
//fonction permettant de valider le email
user.prototype.validateEmail = function () {

    var emailRegEx = /[a-zA-Z0-9]+(\.|_|-)?[a-zA-Z0-9]+@/;
    if (emailRegEx.test(this.email)) {
        return true;
    }
    else {
        return false;
    }

}
//fonction permettant de valider le numero de tel
user.prototype.validateTel = function () {

    var telRegEx = /^(\(\d{3}\)) ?\d{3}-\d{4}/;
    if (telRegEx.test(this.tel)) {
        return true;  //code
    }
    else {

        return false;
    }


}

//fonction permettant de valider le message 
user.prototype.validateMessage = function () {

    var status;

    for (var i = 0; i < this.message.length; i++) {
        if (((this.message.charCodeAt(i) >= "A".charCodeAt(0)) && (this.message.charCodeAt(i) <= "Z".charCodeAt(0))) ||
            ((this.message.charCodeAt(i) >= "a".charCodeAt(0)) && (this.message.charCodeAt(i) <= "z".charCodeAt(0)))) {
            status = true;

        }
        else {
            status = false;
        }
    }

    return status;

}


